export const timerState = {
    RUNNING: 'RUNNING',
    COMPLETE: 'COMPLETE',
    NOT_SET: 'NOT_SET',
};

export const modes = {
    WORK: 'WORK',
    BREAK: 'BREAK',
    LONG_BREAK: 'LONG_BREAK',
    NOT_SET: 'NOT_SET',
};
